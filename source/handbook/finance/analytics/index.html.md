---
layout: markdown_page
title: "Analytics"
---


For more details about our analytics strategy, please see [the biz-ops project](https://gitlab.com/gitlab-org/biz-ops#bizops-project).
