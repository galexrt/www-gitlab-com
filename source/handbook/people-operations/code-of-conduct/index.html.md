---
layout: markdown_page
title: "Code of Conduct"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Other Policies
* [United States Employment Status](/handbook/contracts/#united-states-employment-status)
  * [Pay Period and Working and Keeping Hours](/handbook/finance/#sts=Timesheets)
* [PIAA Agreements](/handbook/contracts/#piaa-agreements)
* [360 Feedback](/handbook/people-operations/360-feedback/)
* [Global Compensation](/handbook/people-operations/global-compensation/)
* [Spending Company Money](/handbook/spending-company-money/)
  * [Return of Property](/handbook/offboarding/#returning-property)
* [Promotions and Transfers](/handbook/people-operations/promotions-transfers/)
* [General Benefits](/handbook/benefits)
* [Entity Specific Benefits](/handbook/benefits/#entity-specific-benefits)
* [Parental Leave](/handbook/benefits/#parental-leave)
* [Paid Time Off](/handbook/paid-time-off/)
* [Anti-Harassment Policy](/handbook/anti-harassment/)
* [Probationary Period](/handbook/contracts/#sts=Probation Period - Confirmation Letter)

## Code of Conduct

This page serves as GitLab's Code of Conduct. It is important for team members to read and fully understand all policies listed and linked from this page. This page is subject to change without notice.

## Code of Ethics

[TODO](https://gitlab.com/gitlab-com/people-ops/General/issues/42)

### Code of Ethics Acknowledgment Form

Team members will review and sign the [Code of Ethics Acknowledgment Form](https://docs.google.com/document/d/1ehxrg-ieUEx1ARZv4LpysMfxzFmnhgtzmJn1-299g_E/edit?usp=sharing) during onboarding as well as annually during the [Global Compensation Annual Review](/handbook/people-operations/global-compensation/#annual-compensation-review) cycle.

## Sick time - Taking and Reporting

In keeping with our [values](/handbook/values) of freedom, efficiency, transparency, kindness, and boring solutions, we have crafted the following protocol around sick leave for all GitLabbers.

**All GitLabbers**

- If you or a loved one is ill, we want you to take care of yourself or your loved one(s). To facilitate this, you should take sick leave when you need it. Sick leave is meant to be used when you are ill, or to care for family members including your parent(s), child(ren), spouse, registered domestic partner, grandparent(s), grandchild(ren), and sibling(s).
- You do need to report when you take sick leave, either by emailing your manager and People Ops, or by using the "Request time off" function in BambooHR. This way, it can be tracked in BambooHR and related payroll systems.
- If you need sick leave for more than 8 consecutive calendar days, notify your manager and People Ops to accommodate an extended leave request. What can (or must) be accommodated varies from location to location: GitLab will comply with the applicable laws in your specific location.
- Upon request, you should be able to provide proper documentation of the reason for your sick leave (doctor's note).

**Details for specific groups of GitLabbers**

- Employees of GitLab Inc. who receive a pay stub from TriNet will see sick time accrue on their pay stub at the rate of 0.0346 hrs per hour worked (3 hours of sick leave per semi-monthly pay-period) for a maximum accrual and carry-over of 72 hours per year. GitLab's policy is more generous than this, in the sense that you can take off non-accrued sick time as written above (a negative balance may show on your pay stub). Sick time does not get paid out in case of termination, nor does it reduce your final paycheck in case of a negative balance. Related to the topic of extended leave requests, see information about [short term disability](/handbook/benefits/#std-ltd) through TriNet / your state.
- Employees of GitLab B.V. have further rights and responsibilities regarding sick time based on Dutch law, as written into their employment [contracts](/handbook/contracts/#employee-contractor-agreements).
- GitLab has engaged with an occupational health and safety centre, Zorg van de Zaak, to assist employees of GitLab B.V. who may need support whilst being on long-term sick leave.
- If an employee is sick People Operations will inform HRSavvy and they will be registered in their HR portal from the first day of sickness.
- If the employee is then on long-term sick leave (1 working week or more) depending on the situation and agreement with the employee, People Operations will instruct HRSavvy to register the employee at the occupational health and safety service.
- Zorg van de Zaak's contact details can be found in the People Operations 1Password vault.

## Worker's Compensation

If you have been injured at work, please contact People Operations to determine what your benefits are.

## Workplace Privacy & Monitoring

Since GitLab is a remote only company, we do not monitor workplaces (e.g., team member's homes). Instead, we monitor access to our infrastructure and/or sensitive data. If you have any questions around privacy/monitoring please reach out to People Operations/Security.

## Alcohol and Drug Policy

While working with federal government clients and customers, team members must be free from the influence of drugs or alcohol. This will help to maintain the efficient and effective operation of the business, and to ensure customers receive the proper service.

GitLab team members must also adhere the local laws of where they reside and where they travel to, including the [GitLab Summit](/culture/summits/).

## Military Leave

GitLab is committed to protecting the position rights of team members absent on military leave. No team member or prospective team member will be subjected to any form of discrimination on the basis of membership in or obligation to perform service for any of the uniformed services of their country of residency. If any team member believes that he or she has been subjected to discrimination in violation of this policy, immediately contact People Operations for assistance. For any questions about how to initiate a military leave, please contact People Operations.

## Hiring Significant Other or Family Members

GitLab is committed to a policy of employment and advancement based on **qualifications and merit** and does not discriminate in favor of or in opposition to the employment of significant others or relatives. Due to the potential for perceived or actual conflicts, such as favoritism or personal conflicts from outside the work environment, which can be carried into the daily working relationship, GitLab will hire or consider other employment actions concerning significant others and/or relatives of persons currently employed or contracted only if:
a) candidates for employment will not be working directly for or supervising a significant other or relative, and
b) candidates for employment will not occupy a position in the same line of authority in which employees can initiate or participate in decisions involving a direct benefit to the significant other or relative. Such decisions include hiring, retention, transfer, promotion, wages, and leave requests.

This policy applies to all current employees and candidates for employment.

## Relocation

If your permanent address is changing, notify People Operations of the new address before the pay cycle of the move.  The best way to do this is by logging in to BambooHR and changing your address under the **Personal** tab. This triggers a message to the BambooHR admin to review the change and "accept" it.

If you are going to spend six months or more in one location this will be considered as a relocation and your compensation will be evaluated based on the new metro region.

- If your relocation is to a different metro area, then to stay aligned with our [compensation principles](/handbook/people-operations/global-compensation-calculator/#compensation-principles) and per the [standard contract agreements](/handbook/contracts), you should obtain written agreement first from your manager and then from People Operations. It is the company's discretion to offer you a contract in your new location. In almost all situations the compensation will change. For an idea about the impact please see our [move calculator](https://about.gitlab.com/roles/move). The move calculator may not always yield accurate results. Run your move past our People Operations Analyst and Chief Culture Officer for an accurate salary in the new [geographical area](/handbook/people-operations/global-compensation/#geographical-areas).
- If the team member is moving to a lower cost of living, the change only needs to be approved by their manager and the Chief Culture Officer. If the team member is moving to a higher cost of living, the People Ops Analyst will escalate to the Chief Culture Officer and the CEO for approval.
- People Ops will check that any necessary changes to payroll and benefits administration are processed in time.
- People Ops will process any changes that are agreed on, and file the email in BambooHR.
- If there are any questions or concerns, please reach out to the Chief Culture Officer.

### Transfer Employee to Different Location in TriNet

1. Go to HR Passport homepage.
1. Click Find.
1. Select find person by Name.
1. Type the name, click search.
1. From the choices, select the name.
1. On the left side of the screen, select Employment Data.
1. Select Employee Transfer.
1. Change location and fill in necessary information.
1. Select Update.

## Tuition Reimbursement

GitLab supports team members who wish to continue their education and growth within their professional career. If you are a full-time GitLabber and have been employed for more than three months, you are eligible to participate in this program.
To be eligible for reimbursement, courses must be a requirement of a degree or certification program and delivered through a credentialed college or university.

GitLabbers are eligible for a reimbursement of up to [4,000 USD](/handbook/people-operations/global-compensation/#exchange-rates) per calendar year (January 1st - December 31st). There is no limit to the number of years a team member can participate in the program. Courses eligible for reimbursement include for credit classes resulting in a grade (not pass/fail), courses providing continuing
education credits, and/or courses taken as part of a certification program. You must earn a passing grade equivalent to a “B” or obtain a successful completion certification to submit for reimbursement. The program will cover only the tuition and enrollment related fees.
Additional fees related to parking, books, supplies, technology, or administrative charges are not covered as part of the program. Tuition will be validated by receipt of payment. A description of the course(s) and degree or certification program along with a
final grade report or satisfactory certificate of completion are required to receive reimbursement.

### Tuition Reimbursement Process

To receive tuition reimbursement, GitLabbers should follow the following process:

1. GitLabber first discusses their interest in professional development with their manager.
1. If the manager agrees that the degree or certification program is aligned with the business and growth opportunities within GitLab, a minimum of three weeks prior to the course start date, the GitLabber fills out a [Tuition Reimbursement Agreement](https://docs.google.com/document/d/1-WvPzoUMpcaUUOm3Gl5eDKQDw_-TtI4J0LP5ifMa8Vo/edit) and forwards it to People Ops to stage for the proper signatures (GitLabber, Manager, People Operations) in HelloSign.
1. The People Ops Analyst will confirm there are no additional [tax implications](/handbook/people-operations/code-of-conduct/#tax-implications-for-tuition-reimbursement-by-country) for reimbursement in the team member's country.
1. People Ops will file the application and signed agreement in BambooHR.
1. People Ops will also log the tuition reimbursement in the "Tuition Reimbursement Log" found on the Google Drive.
1. Once the course is completed, an official grade report or successful certification of completion must be submitted to People Operations.
1. After grades are verified, People Operations will ensure the reimbursement is processed through the applicable payroll by the second pay cycle after submission.

### Tax Implications for Tuition Reimbursement by Country

In some countries, tuition reimbursement may be considered as taxable income.  Please reach out to your tax professional for clarification.

## Reporting Complaints

GitLab has engaged Lighthouse Services to provide an anonymous ethics and compliance hotline for all team members. The purpose of the service is to insure that any team member wishing to submit a report anonymously can do so without the fear of retribution.

Reports may cover but are not limited to the following topics: Ethical violations, Wrongful Discharge, Unsafe Working Conditions, Internal Controls, Quality of Service, Vandalism and Sabotage, [Sexual Harassment](https://about.gitlab.com/handbook/anti-harassment/#sts=Sexual Harassment), Theft, Discrimination, Conduct Violations, Alcohol and Substance Abuse, Threats, Fraud, Bribery and Kickbacks, Conflict of Interest, Improper Conduct, Theft and Embezzlement, Violation of Company Policy, Violation of the Law, Misuse of Company Property, Falsification of Contract, Reports or Records.

Please note that the information provided by you may be the basis of an internal and/or external investigation into the issue you are reporting and your anonymity will be protected to the extent possible by law by Lighthouse. However, your identity may become known during the course of the investigation because of the information you have provided. Reports are submitted by Lighthouse to a company designee for investigation according to our company policies.

Lighthouse Services toll free number and other methods of reporting are available 24 hours a day, 7 days a week for use by team members.   

* Website: [www.lighthouse-services.com/gitlab](https://www.lighthouse-services.com/_StandardCustomURL/incidentLandingPageV3-WorldwideEnglish.asp)
* USA Telephone:
    * English speaking USA and Canada: 833-480-0010
    * Spanish speaking USA and Canada: 800-216-1288
    * French speaking Canada: 855-725-0002
    * Spanish speaking Mexico: 01-800-681-5340
* All other countries telephone: +1-800-603-2869
* E-mail: reports@lighthouse-services.com (must include company name with report)
* Fax: (215) 689-3885 (must include company name with report)

## Mental Health Awareness

1. What is Mental Health?

* The World Health Organisation (WHO) [defines health](https://www.google.co.uk/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0ahUKEwj3oZ7I1M_XAhUsI8AKHXWiC5EQFggmMAA&url=http%3A%2F%2Fwww.who.int%2Fgovernance%2Feb%2Fwho_constitution_en.pdf&usg=AOvVaw0usN4G0kSEScrW3HqENGIr) as:
     * *"a state of complete physical, mental and social well-being and not merely the absence of disease or infirmity.  The enjoyment of the highest attainable standard of health is one of the fundamental rights of every human being without distinction of race, religion, political belief, economic or social condition."*

* The WHO [defines mental health](http://www.who.int/features/factfiles/mental_health/en/) as:
     * *“a state of well-being in which the individual realizes his or her own abilities, can cope with the normal stresses of life, can work productively and fruitfully, and is able to make a contribution to his or her community.”*

* Taking these in turn
     * *"A state of well-being"* is a self-reported measure of 'wellness';
     * *"The individual realizes his or her own abilities"* requires feedback, positive or negative;
     * *"Can cope with the normal stresses of life"* i.e. does not find normal life overwhelming too much of the time;
     * *"Can work productively and fruitfully"* here GitLab clearly has a role to play as it can provide an opportunity for productive and fruitful work;
     * *"Is able to make a contribution to his or her community"* versus the inverse, which is only being able to draw from that community.

1. Why is awareness of Mental Health important at GitLab?
  * It can affect any and all of us.  The statistics from the WHO are that [1 in 4](http://www.who.int/whr/2001/media_centre/press_release/en/) of us will be affected by mental or neurological disorders at some point in our life.  That said, we are all subject to periods where we or those around us find the "the normal stresses of life" harder than usual to deal with.
  * The more aware we are of mental health, the more inclusive we are.  That will help encourage any colleagues currently experiencing mental health issues to talk about it.
  * Our business at its core is a group of people working together towards a common goal.  With awareness of what might affect our colleagues, we are better equipped to help them if they do discuss it with us and therefore help our business.
  * Mental health has so much emotional baggage as a topic that it can initially seem scary to talk about.  Promoting mental health awareness helps to remove the stigma and taboo associated with it.
  * GitLab can offer "productive and fruitful" work for all of our employees.  That should not be [underestimated](https://www.google.co.uk/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwitr7eP18_XAhWIORoKHY2_CA4QFggrMAA&url=https%3A%2F%2Fcardinal-management.co.uk%2Fwp-content%2Fuploads%2F2016%2F04%2FBurton-Waddell-is-work-good-for-you.pdf&usg=AOvVaw0ROXJdWquGML5bIsBEIRLo).
  * In the cold-light of business metrics, the healthier we are, [the more productive we are](https://www.rand.org/randeurope/research/projects/workplace-health-wellbeing-productivity.html).

1. At GitLab we strive to create a Stigma-Free Workplace. In accordance with the National Mental Health Association and the National Council for Behavioral Health we would like to:
  * Educate employees about the signs and symptoms of mental health disorders.
  * Encourage employees to talk about stress, workload, family commitments, and other issues.
  * Communicate that mental illnesses are real, common, and treatable.
  * Discourage stigmatizing language, including hurtful labels such as “crazy,” “loony” or “nuts.”
  * Help employees transition back to work after they take leave.
  * Consult with your employee assistance program.

1. What are we doing to get there?
  * Per an open [issue](https://gitlab.com/gitlab-com/peopleops/issues/138), People Operations will be developing training for managers on this topic.
  * GitLab would also like to encourage GitLabbers to take their [time off](/handbook/paid-time-off) to properly take care of themselves. We encourage the team to go to yoga, take a long lunch, or anything else in their day to day life that assists in their mental and emotional well-being.
  * In addition to our current EAP programs available for employees, we encourage GitLabbers to take a look at [Working Through It](http://www.workplacestrategiesformentalhealth.com/wti/Home.aspx) for insight into reclaiming well-being at work, off work, and return to work.
  * We believe that our values and culture lends itself to being able to discuss mental health open and honestly without being stigmatized, but let's work together to make it even more inclusive.
    * For example, Finding the right words:
      * "How can we help you do your job?"
      * "You’re not your usual self."
      * "Do you want to talk about it?"
      * "It's always OK to ask for help."
      * "It’s hard for me to understand exactly what you’re going through, but I can see that it’s distressing for you."

Any questions or concerns? Please feel free to speak with anyone in People Ops.

## Background Checks

We will obtain employment and criminal background checks for team members based on specific project, client, and/or department assignments. Team members in Support Engineer and Solutions Architect positions, People Ops, Finance, Sales (client-dependent), and the Executive team have been selected to go through this process. Other positions/departments may be added in the future based on business requirements.

We have contracted with [Checkr](https://checkr.com/) to perform these background checks, which will cover criminal history for the last 7 years and employment history for the last 5 years and/or the three most recent employers. GitLab may use the returned background check information to make decisions regarding employment; therefore, the employment of those in the affected positions is contingent upon a successful completion of the background check. Due to the remote nature of our company, offenses involving driving or motor vehicles are not considered actionable violations.

Incoming candidates will receive an email to fill out the background check application following an offer. The application process includes signing a disclosure and a consent form which explains the rights of an individual undergoing a background examination. The application process is designed to take less than fifteen minutes to complete.

All team members in the affected positions will be required to complete the background check process, regardless of location. Applicants outside of the US or non-citizens will be required to provide a passport number and national ID number as part of their criminal background check. At this time, Checkr does not support non-US employment verification, so People Ops will be reaching out to collect the appropriate information.

To prepare for the employment application process, please gather each previous employer's name and address, your position title held, employment start and end dates, manager’s name and title, their phone number, and email address. Details for a Human Resources contact can be entered instead of a manager's contact details. If you have been self-employed, you must provide backup documentation to act as proof of employment. This documentation can be in the form of tax returns (e.g. W2s), pay stubs, LLC documentation, official company registrations, etc.

Applicants may be required to submit a form of picture ID to process their background check. For security purposes, documentation via email will not be accepted; applicants should check their email for the secure link that Checkr will automatically send in order to upload their document.

Background checks will act as an additional mechanism of transparency and will help to build trust with our clients. We will continue to develop this draft policy to ensure we apply a fair and consistent process which is as respectful to the privacy of our team members as possible while remaining compliant.

### Credit Checks
Finance team members **only** will be required to participate in a credit check, due to the nature of their work with company finances. The credit checks will be performed through Checkr.

### Initiating a Background Check

***US Candidates Only***

1. Log in to the Checkr platform and select *Candidates*.
2. Click on *Invite Candidates* and select *Employee Pro Last 3* from the dropdown list. If you need to run a credit check as well, choose *Employee Pro with Credit Report* instead.
3. Enter the candidates personal email address and click *Send Invitations*.

***International Candidates***

1. Repeat the first step from the list above but select *International Criminal Search*.
2. A member of the People Operations team will need to reach out to the candidate to gather previous employer details for employment verification from the candidate directly.
3. If you need to run a credit check as well, repeat the first step and select *Credit Report*.

## Personnel Records

All team members records are kept in [BambooHR](/handbook/people-operations/#sts=Using BambooHR). Team members have self service access to their profile. Where available, documents and information are shared with the team member within the platform. If the team member would like to view their entire profile from the admin view, please schedule a call with People Operations to walk through a screen share or request screenshots to be sent to your personal email address.

## Job Abandonment

When a team member is absent from work for six consecutive workdays, there is no entry on the availability calendar for time off, and fails to contact his or her supervisor, they can be [terminated](/handbook/offboarding/#involuntary-terminations) for job abandonment.

## Confidential Information

At GitLab, our [transparency](/handbook/values/#transparency) allows team members greater access to company information. Team members must honor confidentiality whenever it is stated verbally or in writing.

Please reference your [contract](/handbook/contracts/#employee-contractor-agreements) for topics that are confidential in nature.

## Disciplinary Actions and Procedures

Compliance of each GitLab policy by the team is mandatory. Violations will not be tolerated, and will result in penalties ranging from warnings and reprimands to employment termination. If disciplinary action is suggested, the team member involved will be informed of the alleged violation and given the opportunity to explain their actions.
