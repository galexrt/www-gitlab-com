---
layout: job_page
title: "Senior Director, Data and Analytics"
---

## Responsibilities

* Ensure the Company’s cloud and on-premise data is centralized into a single data warehouse that can support data analysis requirements from all functional groups of the Company.  See our [BizOps effort](https://gitlab.com/gitlab-org/bizops).
* Create a common data framework so that all company data can be analyzed in a unified manner.
* Design, implement, and manage the organization's go-to-market systems archetecture, including the sales and marketing infrastructure accross Marketing, Sales, Customer Success and Accounting used to support the customer journey.
* Create and execute a plan to develop and mature our ablility to measure and optimize usage growth and our [user journey](https://about.gitlab.com/handbook/journeys/#user-journey).
* Ensure that all transactional systems can communicate with each other either directly or via the data warehouse and that production data adheres to a unified data model.
* Ensure that each metric in the Company’s dashboard has a single source of Truth and that data ownership and validation are incorporated on a consistent basis.
* Determine the level of integration necessary between transactional systems to deliver the right data in the right context to users.
* Develop a roadmap for system expansion, evaluate existing systems and ensure future systems are aligned with the Company’s data architecture plan which you will largely help develop.
* Implement a set of processes that that ensure any changes in transactional system architecture are documented and their impact on the company’s overall data integrity are considered prior to changes being made.
* Collaborate with all functions of the company to ensure data needs are addressed and system.
* This position reports directly to the CFO and works closely with the executive team to develop an organization plan that addresses company wide analytic resources in either a direct report or matrix model.


## Requirements

* Minimum 4 years hands on experience in a data analytics role
* Experience with a high growth company using on-premise tools and on-demand (SaaS) transactional systems
* Hands on experience with Python, MySQL, SQL, ETL tools.  Experience with Postgres is a plus.
* Have previously lead a corporate data platform project
* Experience with open source data warehouse tools
* Experience working with multiple executive level business stake holders
* Experience with analytic and data visualization tools such as Tableau, Looker, etc
* Experience with Salesforce, Zuora, Zendesk and Marketo
* Share and work in accordance with our values
* Must be able to work in alignment with Americas timezones
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).


## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our CFO
* Candidates will then be invited to schedule a second interview with our CRO, CMO and Director of Demand Generation
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).



