## Senior Developers

Senior Developers are experienced developers who meet the following requirements:

1. Technical skills
    * Write modular, well-tested, and maintainable code
    * Know a domain really well and radiate that knowledge
    * Contribute to one or more complementary projects
    * Take on difficult issues, big or small
2. Leadership
    * Begin to show architectural perspective
    * Propose new ideas, performing feasibility analyses and scoping the work
    * Capable of leading complicated features, bug fixes, and integrations with
      limited guidance from leads
    * Should make the entire team more productive through their efforts. This 
      _might_ be through mentoring junior developers, or by improving the 
      team's process, documentation, testing or tooling in a way that helps 
      everyone in the team be more effective.
3. Code quality
    * Leave code in substantially better shape than before
    * Keep complexity contained as much as possible: if a feature is complex,
      that's sometimes unavoidable, but Senior Developers show that they can
      reduce or mitigate complexity in their changes
    * Fix bugs/regressions quickly
    * Monitor overall code quality/build failures
    * Leave tests in better shape than before: faster, more reliable, more
      comprehensive, etc.
4. Communication
    * Provide thorough and timely code feedback for peers, leaving minimal work
      for later reviews / maintainers
    * Able to communicate clearly on technical topics and present ideas to the
      rest of the team
    * Keep issues up-to-date with progress
    * Help guide other merge requests to completion
5. Performance and scalability
    * Excellent at writing production-ready code with little assistance
    * Are able to write complex code that can scale with a significant number of users
    * Are able to fix performance issues on GitLab.com with minimal guidance using
      our existing tools, and improve those tools where needed
