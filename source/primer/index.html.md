---
layout: markdown_page
title: "GitLab Primer"
---

Read the pages below to learn more about GitLab:

1. [About](https://about.gitlab.com/about/)
1. [Strategy](https://about.gitlab.com/strategy/#sequence)
1. [Direction](https://about.gitlab.com/direction/#scope)
1. [Handbook](https://about.gitlab.com/handbook/)
1. [Team](https://about.gitlab.com/team/)
1. [Features](https://about.gitlab.com/features/)
1. [SDLC](https://about.gitlab.com/sdlc/)
1. [Values](https://about.gitlab.com/handbook/values/)
1. [Culture](https://about.gitlab.com/culture/)
1. [OKRs](https://about.gitlab.com/okrs/)
1. [Is it any good?](https://about.gitlab.com/is-it-any-good/)
